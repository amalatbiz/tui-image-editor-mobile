# Tui-Image-Editor-Mobile-View
---
![Tui-Image-Editor Mobile-View Screenshot](./img/tui-editor-mobile-ss.bmp)

## Reference
Tui-Image-Editor-Mobile-View is a subset of [tui-image-editor mobile-view example repo](https://github.com/nhn/tui.image-editor/blob/master/apps/image-editor/examples/example03-mobile.html), which contains only the files and dependencies required for mobile view of [tui-image-editor](https://github.com/nhn/tui.image-editor/tree/master/apps/image-editor/src).

---
>## TODO
> Modify this repo to consume in Angular.